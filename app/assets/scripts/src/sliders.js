//////////////////////
////////////////////// Common Slider
//////////////////////
(function(window, document, $){
	"use strict";

	// check for NinjaSlider dependency
	if( typeof NinjaSlider === 'undefined' ){
		console.log('Error: NinjaSlider is required');
		return;
	}

	var CommonSlider = function( element ){
		this.container = element;
		this.$container = $(element);
		this.$slides = this.$container.find('[data-role="slider-slide"]');
		this.$arrows = this.$container.find('[data-role="slider-arrow"]');
		this.$bullets = this.$container.find('[data-role="slider-bullet"]');
		this.$thumbnails = this.$container.find('[data-role="slider-thumbnail"]');
		this.$feet = this.$container.parent().find('[data-role="slider-feet"]');
		this.$tags = this.$container.parent().find('[data-role="slider-tag"]');
		this.$captions = this.$container.parents('[data-role="slider-holder"]').find('[data-role="slider-caption"]');
		this.$transition = this.$container.data('transition') || false;
		this.$speed = this.$container.data('speed') || 400;
		this.$autotime = 2000;
		this.$slidelist = this.$container.find('[data-role="slider-list"]');
		this.$slideholder = $(this.$container.parents('[data-role="slider-holder"]'));
		this.$slidefooter = this.$slideholder.find('[data-role="slider-footer"]');

		if(typeof this.$transition !== 'undefined'){
			this.$autotime = this.$transition;
		}

		this.$slidelist.find('.slider__slide:first-child').addClass('current').attr('data-current', true);
		this.$slidefooter.find('.slider__caption:first-child').addClass('current');

		this.ninjaSlider = new NinjaSlider(this.container, {
			auto: this.$autotime,
			speed: this.$speed,
			transitionCallback: this.transitionCallback.bind(this)
		});

		this.$arrows.on('click.CommonSlider', this.arrowCallback.bind(this));
		this.$bullets.on('click.CommonSlider', this.bulletCallback.bind(this));
		this.$thumbnails.on('click.CommonSlider', this.bulletCallback.bind(this));
		this.$tags.on('click.CommonSlider', this.tagCallback.bind(this));
		this.$captions.on('click.CommonSlider', this.captionCallback.bind(this));
	};
	CommonSlider.prototype = {
		transitionCallback : function( index, slide, ninjaSlider ){
			if( this.$bullets.length ){
				this.$bullets.removeClass('current');
				this.$bullets.filter('[data-target="'+ index +'"]').addClass('current');
			}	

			if( this.$thumbnails.length ){
				this.$thumbnails.removeClass('current');
				this.$thumbnails.filter('[data-target="'+ index +'"]').addClass('current');
			}

			if( this.$captions.length ){
				this.$captions.removeClass('current');
				this.$captions.filter('[data-target="'+ index +'"]').addClass('current');
			}

			if( this.$slides.length ){
				this.$slides.removeClass('current').attr('data-current', false);
				this.$slides.filter('[data-index="'+ index +'"]').addClass('current').attr('data-current', true);
			}
		},

		arrowCallback : function( event ){
			event.preventDefault();
			var $btn = $(event.currentTarget),
				direction = $btn.data('direction');

			this.ninjaSlider.reset();
			this.ninjaSlider[ direction ]();
		},

		bulletCallback : function( event ){
			event.preventDefault();
			var $btn = $(event.currentTarget),
				target = $btn.data('target');

			this.ninjaSlider.reset();
			this.ninjaSlider.slide( target );
		},

		tagCallback : function( event ){
			event.preventDefault();
			var $btn = $(event.currentTarget),
				target = $btn.data('target');

			this.ninjaSlider.reset();
			this.ninjaSlider.slide( target );
		},

		captionCallback : function( event ){
			event.preventDefault();
			var $btn = $(event.currentTarget),
				target = $btn.data('target');

			this.ninjaSlider.reset();
			this.ninjaSlider.slide( target );
		}
	};

	$.fn.commonSlider = function(){
		if( this.data('commonSlider') ){ return this.data('commonSlider'); }
		return this.each(function(i, el){
			$(el).data('commonSlider', (new CommonSlider(el)));
		});
	};

	// self initialization
	$(document).ready(function(){
		$('[data-module="slider"]').commonSlider();
	});
}(window, document, jQuery));