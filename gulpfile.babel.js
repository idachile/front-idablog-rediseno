'use strict';

// This gulpfile makes use of new JavaScript features.
// Babel handles this without us having to do anything. It just works.
// You can read more about the new JavaScript features here:
// https://babeljs.io/docs/learn-es2015/

import gulp from 'gulp';
import pkg from './package.json';
import sftp from 'gulp-sftp';

import runSequence from 'run-sequence';
import browserSync from 'browser-sync';
const browsersync = require('browser-sync').create();
const reload = browserSync.reload;

import gulpLoadPlugins from 'gulp-load-plugins';
const $ = gulpLoadPlugins();

const ssi = require('browsersync-ssi');
const includer = require('gulp-html-ssi');
const ext_replace = require('gulp-ext-replace');
const string_replace = require('gulp-string-replace');
const imagemin = require('gulp-imagemin');
var rename = require("gulp-rename");
var htmlbeautify = require('gulp-html-beautify');
const app_path = './app';


// Optimize images
gulp.task('images', () =>
   gulp.src(app_path + '/assets/images/**/*')
   .pipe($.imagemin())
   .pipe(gulp.dest(app_path + '/dist/img'))
   .pipe($.size({
      title: 'images'
   }))
);

// Compile and automatically prefix stylesheets
gulp.task('styles', () => {
   const AUTOPREFIXER_BROWSERS = [
      'ie >= 10',
      'ie_mob >= 10',
      'ff >= 30',
      'chrome >= 34',
      'safari >= 7',
      'opera >= 23',
      'ios >= 7',
      'android >= 4.4',
      'bb >= 10'
   ];

   // For best performance, don't add Sass partials to `gulp.src`
   return gulp.src([
         app_path + '/assets/styles/main.scss',
      ])
      .pipe($.sourcemaps.init())
      .pipe($.sass({
         precision: 10
      }).on('error', $.sass.logError))
      .pipe($.autoprefixer(AUTOPREFIXER_BROWSERS, {
         grid: true
      }))
      .pipe($.if('*.css', $.cssnano({
         discardComments: {
            removeAll: true
         }
      })))
      .pipe($.size({
         title: 'styles'
      }))
      .pipe($.sourcemaps.write('./'))
      .pipe(gulp.dest(app_path + '/dist/css'));
});


//Styles watch
gulp.task('styles:watch', ['styles'], () => {
   gulp.watch(app_path + '/assets/styles/**/*', ['styles']);
});




////////
//scripts a incluir 

gulp.task('slider_js', () =>
   gulp.src([
      app_path + '/assets/scripts/libs/ninjaSlider-2.0.js',
      app_path + '/assets/scripts/src/sliders.js',
   ])
   .pipe($.sourcemaps.init())
   .pipe($.babel())
   .pipe($.concat('sliders.js'))
   .pipe($.uglify({
      compress: true
   }))
   .pipe($.size({
      title: 'slider_js'
   }))
   .pipe($.sourcemaps.write('.'))
   .pipe(gulp.dest(app_path + '/dist/js/resource/'))
);

gulp.task('carousel_js', () =>
   gulp.src([
      app_path + '/assets/scripts/libs/ninjaSlider-2.0.js',
      app_path + '/assets/scripts/src/carousel.js',
   ])
   .pipe($.sourcemaps.init())
   .pipe($.babel())
   .pipe($.concat('carousel.js'))
   .pipe($.uglify({
      compress: true
   }))
   .pipe($.size({
      title: 'carousel_js'
   }))
   .pipe($.sourcemaps.write('.'))
   .pipe(gulp.dest(app_path + '/dist/js/resource/'))
);

gulp.task('forms_js', () =>
   gulp.src([
      app_path + '/assets/scripts/libs/validizr-3.0.js',
      app_path + '/assets/scripts/src/forms.js',
      app_path + '/assets/scripts/src/messages.js',
   ])
   .pipe($.sourcemaps.init())
   .pipe($.babel())
   .pipe($.concat('forms.js'))
   .pipe($.uglify({
      compress: true
   }))
   .pipe($.size({
      title: 'forms_js'
   }))
   .pipe($.sourcemaps.write('.'))
   .pipe(gulp.dest(app_path + '/dist/js/resource/'))
);

gulp.task('modal_js', () =>
   gulp.src([
      app_path + '/assets/scripts/src/modals.js',
   ])
   .pipe($.sourcemaps.init())
   .pipe($.babel())
   .pipe($.concat('modals.js'))
   .pipe($.uglify({
      compress: true
   }))
   .pipe($.size({
      title: 'modal_js'
   }))
   .pipe($.sourcemaps.write('.'))
   .pipe(gulp.dest(app_path + '/dist/js/resource/'))
);

gulp.task('sticky_js', () =>
   gulp.src([
      app_path + '/assets/scripts/src/sticky.js',
   ])
   .pipe($.sourcemaps.init())
   .pipe($.babel())
   .pipe($.concat('sticky.js'))
   .pipe($.uglify({
      compress: true
   }))
   .pipe($.size({
      title: 'sticky_js'
   }))
   .pipe($.sourcemaps.write('.'))
   .pipe(gulp.dest(app_path + '/dist/js/resource/'))
);

gulp.task('scripts', ['slider_js', 'forms_js', 'modal_js', 'sticky_js'], () =>
   gulp.src([
      app_path + '/assets/scripts/libs/modernizr-3.3.1.js',
      app_path + '/assets/scripts/src/enquire.js',
      app_path + '/assets/scripts/src/mq-changes.js',
      app_path + '/assets/scripts/src/nav-bar.js',
      app_path + '/assets/scripts/app.js',
      app_path + '/assets/scripts/general.js',
   ])
   .pipe($.sourcemaps.init())
   .pipe($.babel())
   .pipe($.concat('main.js'))
   .pipe($.uglify({
      compress: true
   }))
   // Output files
   .pipe($.size({
      title: 'scripts'
   }))
   .pipe($.sourcemaps.write('.'))
   .pipe(gulp.dest(app_path + '/dist/js'))
);
//Styles watch
gulp.task('scripts:watch', ['scripts'], () => {
   gulp.watch(app_path + '/assets/scripts/**/*', ['scripts']);
});


///////////////////////////////////////////

//Concatenate and convert shtml to HTML files
//(include .html links)
gulp.task('shtml', () => {
   gulp.src(app_path + '/**/**/*.shtml')
      .pipe(includer())
      .pipe(ext_replace('.html'))
      .pipe(string_replace('.shtml', '.html'))
      .pipe(string_replace('http://', 'https://'))
      .pipe(gulp.dest('./front'));
});

gulp.task('https', () => {
   gulp.src(app_path + '/**/**/*.shtml')
      .pipe(string_replace('http://', 'https://'))
      .pipe(gulp.dest('./app'));
});

gulp.task('copy', () => {
   gulp.src([
         app_path + '/dist/**/*',
         // app_path + '/partials/**/*',
      ], {
         base: 'app'
      })
      .pipe(gulp.dest('./front'));
});

gulp.task('front', ['copy', 'shtml'], () => {});



/*************************** */
/*************************** */
/****Servers****** */
/*************************** */
/*************************** */



//Watch using shtml
// gulp.task('virtual', ['styles', 'scripts', 'images'], () => {
gulp.task('virtual', ['styles', 'scripts'], () => {
   browserSync({
      middleware: ssi({
         baseDir: 'app/',
         baseUrl: `http://localhost:3004`,
         ext: '.shtml',
         version: '1.4.0'
      }),
      notify: false,
      logPrefix: '**WSK**',
      server: ['app'],
      port: 3009,
      https: false,
      browser: "google chrome"
   });
   gulp.watch([app_path + '/**/*.shtml'], reload);
   gulp.watch([app_path + '/assets/styles/**/**/*.{scss,css}'], ['styles', reload]);
   gulp.watch([app_path + '/assets/scripts/libs/*.js', app_path + '/assets/scripts/src/*.js', app_path + '/assets/scripts/*.js'], ['scripts', reload]);
});


gulp.task('compile', ['styles', 'scripts', 'images', ], () => {});


gulp.task('front', ['copy', 'shtml'], () => {});

gulp.task('beauty', () => {
   gulp.src('./front/*.html')
      .pipe(htmlbeautify({
         indent: 3,
         "indent_with_tabs": true,
         "preserve_newlines": false
      }))
      .pipe(gulp.dest('./front/'))
});