/*
    Anade eventos especiales "scrollstart" y "scrollstop"
*/
(function(e){var t=e.event.dispatch||e.event.handle;var n=e.event.special,r="D"+ +(new Date),i="D"+(+(new Date)+1);n.scrollstart={setup:function(i){var s=e.extend({latency:n.scrollstop.latency},i);var o,u=function(e){var n=this,r=arguments;if(o){clearTimeout(o)}else{e.type="scrollstart";t.apply(n,r)}o=setTimeout(function(){o=null},s.latency)};e(this).bind("scroll",u).data(r,u)},teardown:function(){e(this).unbind("scroll",e(this).data(r))}};n.scrollstop={latency:250,setup:function(r){var s=e.extend({latency:n.scrollstop.latency},r);var o,u=function(e){var n=this,r=arguments;if(o){clearTimeout(o)}o=setTimeout(function(){o=null;e.type="scrollstop";t.apply(n,r)},s.latency)};e(this).bind("scroll",u).data(i,u)},teardown:function(){e(this).unbind("scroll",e(this).data(i))}}})(jQuery);

(function(window, $){
    "use strict";

    var $window = $(window);

    /// 
    /// var callbacks es un objeto con los metodos "engage" y "disengage"

    window.NinjaStickyBox = function( box, position_offset, callbacks ){
        var self = this;

        self.$element = $(box);
        self.parentSelector = self.$element.data('stickybox');
        self.$parent = $( self.parentSelector );

        self.isScrolling = false;
        self.position_offset = position_offset || 0;
        self.fixed_class = 'sticky-box-fixed';

        self.callbacks = callbacks || {};

        self.setVars();
        self.init();
    };
    window.NinjaStickyBox.prototype = {
        init : function(){
            var self = this;
            $window.on('scroll scrollstart scrollstop', $.proxy( self.handleScroll, self ));
            $window.on('resize', $.proxy( self.handleResize, self ));
            self.handleMovement();
        },
        setVars : function(){
            var self = this;
            self.element_offset = self.$element.offset();
            self.element_width = self.$element.outerWidth();
            self.element_height = self.$element.outerHeight();

            self.parentOffset_top = self.$parent.offset().top;
            self.parentOffset_bottom = self.getBottomOffset( self.$parent );

            self.scrollTop = $window.scrollTop();

            self.critial_pos = self.parentOffset_top - self.position_offset;
        },
        getBottomOffset : function( $obj ){
            return $obj.offset().top + $obj.outerHeight();
        },
        handleResize : function(){
            var self = this;

            self.$element.removeClass( self.fixed_class ).css({
                'width' : 'auto',
                'left' : 'auto',
                'top' : 'auto'
            });
            self.setVars();
            self.handleMovement();
        },
        handleScroll : function( scrollEvent ){
            var self = this;

            self.scrollTop = $window.scrollTop();

            if( scrollEvent.type === 'scrollstart' ){
                self.isScrolling = true;
                self.handleMovement();
            }
            else if( scrollEvent.type === 'scrollstop' ) {
                self.isScrolling = false;
            }
        },
        handleMovement : function(){
            var self = this,
                relativeBottomOffset = self.scrollTop + self.position_offset + self.element_height,
                bottomReached = relativeBottomOffset >= self.parentOffset_bottom,
                props, relativeOffsetCalc;

            if( (self.scrollTop >= self.critial_pos) && !bottomReached ){
                props = {
                    'width' : self.element_width + 'px',
                    'left' : self.element_offset.left + 'px',
                    'top' : self.position_offset + 'px'
                };
                self.$element.addClass( self.fixed_class );
                self.callbacks.engage && self.callbacks.engage( self.$element );
            } 
            else if( bottomReached ){
                relativeOffsetCalc = self.position_offset - ( relativeBottomOffset - self.parentOffset_bottom );
                props = {
                    'width' : self.element_width + 'px',
                    'left' : self.element_offset.left + 'px',
                    'top' : relativeOffsetCalc + 'px'
                };
                self.$element.addClass( self.fixed_class );
                self.callbacks.engage && self.callbacks.engage( self.$element );
            }
            else {
                props = {'width' : 'auto', 'left' : 'auto', 'top' : 'auto'};
                self.$element.removeClass( self.fixed_class );
                self.callbacks.disengage && self.callbacks.disengage( self.$element );
            }

            self.$element.css( props );

            if( self.isScrolling ) {
                requestAnimationFrame(function(){ 
                    self.handleMovement(); 
                });
            }
        }
    };


    $.fn.ninjaStickyBox = function(offset, fixed_class){
        return new window.NinjaStickyBox( this, offset, fixed_class );
    };

}(this, jQuery));